<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220310064348 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE marfa (id INT AUTO_INCREMENT NOT NULL, nume VARCHAR(255) NOT NULL, descriere VARCHAR(255) DEFAULT NULL, data_expirari DATETIME NOT NULL, fragil TINYINT(1) NOT NULL, greutate DOUBLE PRECISION NOT NULL, volum INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE marfa');
        $this->addSql('ALTER TABLE depozit CHANGE nume nume VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE locatie locatie VARCHAR(255) NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
